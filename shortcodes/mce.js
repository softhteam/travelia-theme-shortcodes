(function() {
	tinymce.PluginManager.add('travelia_mce_button', function( editor, url ) {
		editor.addButton( 'travelia_mce_button', {
            text: traveliaLang.shortcode_travelia, 
            icon: false,
			tooltip: traveliaLang.shortcode_travelia,
			type: 'menubutton',
			minWidth: 210,
			menu: [
				{
					text: traveliaLang.shortcode_button,
					onclick: function() {
						editor.windowManager.open( {
							title: traveliaLang.shortcode_button,
							body: [
								{
									type: 'listbox',
									name: 'ButtonType',
									label: traveliaLang.shortcode_type,
									'values': [
										{text: traveliaLang.shortcode_default, value: 'btn-default'},
										{text: traveliaLang.shortcode_primary, value: 'btn-primary'},
										{text: traveliaLang.shortcode_success, value: 'btn-success'},
										{text: traveliaLang.shortcode_info, value: 'btn-info'},
										{text: traveliaLang.shortcode_warning, value: 'btn-warning'},
										{text: traveliaLang.shortcode_danger, value: 'btn-danger'},
									]
								},
								{
									type: 'listbox',
									name: 'ButtonSize',
									label: traveliaLang.shortcode_size,
									'values': [
										{text: traveliaLang.shortcode_size_large, value: 'btn-lg'},
										{text: traveliaLang.shortcode_size_default, value: ''},
										{text: traveliaLang.shortcode_size_small, value: 'btn-sm'},
										{text: traveliaLang.shortcode_size_ex_small, value: 'btn-xs'},
									]
								},
								{
									type: 'textbox',
									name: 'ButtonText',
									label: traveliaLang.shortcode_text,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[button type="' + e.data.ButtonType + '" size="' + e.data.ButtonSize + '" text="' + e.data.ButtonText + '"]');
							}
						});
					} // onclick
				},
				{
					text: traveliaLang.shortcode_progressbar,
					onclick: function() {
						editor.windowManager.open( {
							title: traveliaLang.shortcode_progressbar,
							body: [
								{
									type: 'listbox',
									name: 'ProgressbarType',
									label: traveliaLang.shortcode_type,
									'values': [
										{text: traveliaLang.shortcode_default, value: 'default'},
										{text: traveliaLang.shortcode_primary, value: 'primary'},
										{text: traveliaLang.shortcode_success, value: 'success'},
										{text: traveliaLang.shortcode_info, value: 'info'},
										{text: traveliaLang.shortcode_warning, value: 'warning'},
										{text: traveliaLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'textbox',
									name: 'ProgressbarWidth',
									label: traveliaLang.shortcode_width,
									minWidth: 300,
									value: '50'
								},
								{
									type: 'textbox',
									name: 'ProgressbarTitle',
									label: traveliaLang.shortcode_title,
									minWidth: 300,
									value: ''
								},
								{
									type: 'listbox',
									name: 'ProgressbarStriped',
									label: traveliaLang.shortcode_striped,
									'values': [
										{text: traveliaLang.shortcode_true, value: 'true'},
										{text: traveliaLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'listbox',
									name: 'ProgressbarAnimation',
									label: traveliaLang.shortcode_animation,
									'values': [
										{text: traveliaLang.shortcode_true, value: 'true'},
										{text: traveliaLang.shortcode_false, value: 'false'},
									]
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[progress_bar type="' + e.data.ProgressbarType + '" width="' + e.data.ProgressbarWidth + '" title="' + e.data.ProgressbarTitle + '" striped="' + e.data.ProgressbarStriped + '" animation="' + e.data.ProgressbarAnimation + '"]');
							}
						});
					}
				},
				{
					text: traveliaLang.shortcode_status,
					onclick: function() {
						editor.windowManager.open( {
							title: traveliaLang.shortcode_status,
							body: [
								{
									type: 'listbox',
									name: 'StatusType',
									label: traveliaLang.shortcode_type,
									'values': [
										{text: traveliaLang.shortcode_facebook, value: 'facebook'},
										{text: traveliaLang.shortcode_twitter, value: 'twitter'},
										{text: traveliaLang.shortcode_google_plus, value: 'google_plus'},
									]
								},
								{
									type: 'textbox',
									name: 'StatusURL',
									label: traveliaLang.shortcode_url,
									minWidth: 300,
									value: ''
								},
								{
									type: 'textbox',
									name: 'StatusBackground',
									label: traveliaLang.shortcode_background_img,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[status type="' + e.data.StatusType + '" url="' + e.data.StatusURL + '" background="' + e.data.StatusBackground + '"]');
							}
						});
					}
				},
				{
					text: traveliaLang.shortcode_alert,
					onclick: function() {
						editor.windowManager.open( {
							title: traveliaLang.shortcode_alert,
							body: [
								{
									type: 'listbox',
									name: 'AlertType',
									label: traveliaLang.shortcode_type,
									'values': [
										{text: traveliaLang.shortcode_default, value: 'default'},
										{text: traveliaLang.shortcode_primary, value: 'primary'},
										{text: traveliaLang.shortcode_success, value: 'success'},
										{text: traveliaLang.shortcode_info, value: 'info'},
										{text: traveliaLang.shortcode_warning, value: 'warning'},
										{text: traveliaLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'listbox',
									name: 'AlertDismiss',
									label: traveliaLang.shortcode_dismiss,
									'values': [
										{text: traveliaLang.shortcode_true, value: 'true'},
										{text: traveliaLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'textbox',
									name: 'AlertContent',
									label: traveliaLang.shortcode_content,
									value: '',									
									multiline: true,
									minWidth: 300,
									minHeight: 100
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[alert type="' + e.data.AlertType + '" dismiss="' + e.data.AlertDismiss + '"]' + e.data.AlertContent + '[/alert]');
							}
						});
					}
				},
				{
					text: traveliaLang.shortcode_video,
					onclick: function() {
						editor.windowManager.open( {
							title: traveliaLang.shortcode_video,
							body: [
								{
									type: 'textbox',
									name: 'VideoURL',
									label: traveliaLang.shortcode_video_url,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'VideoWidth',
									label: traveliaLang.shortcode_width,
									value: ''
								},
								{
									type: 'textbox',
									name: 'Videoheight',
									label: traveliaLang.shortcode_height,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[embed width="' + e.data.VideoWidth + '" height="' + e.data.Videoheight + '"]' + e.data.VideoURL + '[/embed]');
							}
						});
					}
				},
				{
					text: traveliaLang.shortcode_audio,
					onclick: function() {
						editor.windowManager.open( {
							title: traveliaLang.shortcode_audio,
							body: [
								{
									type: 'textbox',
									name: 'mp3URL',
									label: traveliaLang.shortcode_mp3,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'm4aURL',
									label: traveliaLang.shortcode_m4a,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'oggURL',
									label: traveliaLang.shortcode_ogg,
									value: 'http://',
									minWidth: 300,
								},
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[audio mp3="' + e.data.mp3URL + '" m4a="' + e.data.m4aURL + '" ogg="' + e.data.oggURL + '"]');
							}
						});
					}
				},
				{
					text: traveliaLang.shortcode_tooltip,
					onclick: function() {
						editor.windowManager.open( {
							title: traveliaLang.shortcode_tooltip,
							body: [
								{
									type: 'textbox',
									name: 'TooltipText',
									label: traveliaLang.shortcode_text,
									value: '',
									minWidth: 300,
								},
								{
									type: 'listbox',
									name: 'TooltipDirection',
									label: traveliaLang.shortcode_direction,
									'values': [
										{text: traveliaLang.shortcode_top, value: 'top'},
										{text: traveliaLang.shortcode_right, value: 'right'},
										{text: traveliaLang.shortcode_bottom, value: 'bottom'},
										{text: traveliaLang.shortcode_left, value: 'left'},
									]
								},
								{
									type: 'textbox',
									name: 'ToolTipContent',
									label: traveliaLang.shortcode_content,
									value: '',
									multiline: true,
									minWidth: 300,
									minHeight: 100
								}
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[tooltip text="' + e.data.TooltipText + '" direction="' + e.data.TooltipDirection + '"]'+ e.data.ToolTipContent +'[/tooltip]');
							}
						});
					}
				},
				{
					text: traveliaLang.shortcode_columns,
						onclick: function() {
							editor.insertContent( '\
							[row]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
							[/row]');
						}
				},
				{
					text: traveliaLang.shortcode_accordion,
						onclick: function() {
							editor.insertContent( '\
							[collapse_group]<br />\
								[collapse id="accordion_one" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
								[collapse id="accordion_two" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
							[/collapse_group]');
						}
				},
				{
					text: traveliaLang.shortcode_tab,
						onclick: function() {
							editor.insertContent( '\
							[tabs]<br />\
								[tab id="tab_one" title="Tab Title" active="active"] Tab Content [/tab]<br />\
								[tab id="tab_two" title="Tab Title"] Tab Content [/tab]<br />\
								[tab id="tab_three" title="Tab Title"] Tab Content [/tab]<br />\
							[/tabs]');
						}
				},
				{
                    text: traveliaLang.shortcode_custom_icon,
                    classes: 'sh-custom-icon',
                },
			]
		});
	});
})();